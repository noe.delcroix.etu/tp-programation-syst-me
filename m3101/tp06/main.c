#include "main.h"
#include "fonctions.h"
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char **argv){

	if (argc>3){
		error("Trop d'arguments\n");
		return -1;
	}
	int source,destination;

	if (argc==3){
		source=open(argv[1], O_RDONLY);
		destination=open(argv[2],O_WRONLY|O_CREAT,0644);
	}else if (argc==2){
		source=open(argv[1], O_RDONLY);
		destination=1;
	}else if (argc==1){
		printf("Entrez un texte à convertir en base 64 :\n");
		source=0;
		destination=1;
	}

	encoder_fichier(source,destination,150);
	//printf("\n");

	close(destination);
	close(source);
	return 0;
}


/*int main(void){
	
	char* source="aaaaaaaaaaa";
	char* destination="                 \0";
	
	encoder_packets(source,12,destination);
}*/