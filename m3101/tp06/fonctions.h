#ifndef FONCTIONS_H
#define FONCTIONS_H

char convertir(char valeur);
void encoder_bloc(const char *source, int taille_source, char *destination, int paquet);
int encoder_packets(const char *source, int taille_source, char *destination);
int encoder_fichier(int source, int destination,int nbBlocsDeTrois);
int error(char* message);

#endif