#include "fonctions.h"
#include <stdio.h>
#include <unistd.h>

char convertir(char valeur){
	//printf("%d (%c)\n",valeur,valeur);

	//A -> Z
	if (valeur>=0 && valeur<=25){
		return valeur+'A'-0;
	}

	//a -> z
	if (valeur>=26 && valeur<=51){
		return valeur+'a'-26;
	}

	//0 -> 9
	if (valeur>=52 && valeur<=61){
		return valeur+'0'-52;
	}

	//+
	if (valeur==62){
		return '+';
	}

	//-
	if (valeur==63){
		return '/';
	}

	return -1;
}


void encoder_bloc(const char *source, int taille_source, char *destination, int paquet){

	//printf("Encodage : %d\n",paquet);
	
	//printf("Source (%d) -> <%c%c%c>\n",taille_source,source[0+paquet*3],source[1+paquet*3],source[2+paquet*3]);
	

    
	if(taille_source==3)
    {
        destination[0+paquet*4]=convertir(source[0+paquet*3]>>2 & 0b111111);
        destination[1+paquet*4]=convertir(((source[0+paquet*3] & 0b00000011) << 4) | (source[1+paquet*3]>>4 & 0b00001111));
        destination[2+paquet*4]=convertir(((source[1+paquet*3] & 0b00001111) << 2) | (source[2+paquet*3]>>6 & 0b00000011));
        destination[3+paquet*4]=convertir(source[2+paquet*3] & 0b111111);
    }
    if(taille_source==2)
    {
        destination[0+paquet*4]=convertir(source[0+paquet*3]>>2 & 0b00111111);
        destination[1+paquet*4]=convertir(((source[0+paquet*3] & 0b00000011) << 4) | (source[1+paquet*3]>>4 & 0b00001111));
        destination[2+paquet*4]=convertir((source[1+paquet*3] & 0b00001111) << 2);
        destination[3+paquet*4]='=';
    }
    if(taille_source==1)
    {
        destination[0+paquet*4]=convertir(source[0+paquet*3]>>2 & 0b111111);
        destination[1+paquet*4]=convertir((source[0+paquet*3] & 0b11) << 4);
        destination[2+paquet*4]='=';
        destination[3+paquet*4]='=';
    }


	//printf("Destination -> <%c%c%c%c>\n",destination[0+paquet*4],destination[1+paquet*4],destination[2+paquet*4],destination[3+paquet*4]);
	//printf("Positions -> <%d,%d,%d,%d>\n",0+paquet*4,1+paquet*4,2+paquet*4,3+paquet*4);

}

int encoder_packets(const char *source, int taille_source, char *destination){
    
	source=source;
	destination=destination;

	int taillePacket=3;
	int paquet=0;

	while (taillePacket==3){
		if ((paquet+1)*3<taille_source){
			taillePacket=3;
		}else{
			taillePacket=taille_source- ( (paquet)*3);
		}
		if (taillePacket==0) break;

		//printf("%d",taillePacket);
		encoder_bloc(source,taillePacket,destination,paquet);
		//printf("-------------------------\n");
		paquet++;
	}

	return paquet;

	//printf("==========================\n");
}


/*void encoder_bloc(const char *source, int taille_source, char *destination){
    char var[3];
    var[0] = source[0];
    var[1] = 0;
    var[2] = 0;
    if(taille_source==2){
        var[1] = source[1];
    }else if(taille_source==3){
        var[1] = source[1];
        var[2] = source[2];
    }

    int D = (var[0] >> 2) & 0b00111111;
    int E = ((var[0] & 0b00000011)<<4) | ((var[1] >> 4) & 0b00001111) ;
    int F = ((var[1] & 0b00001111)<<2) | ((var[2] >> 6) & 0b00000011) ;
    int G = (var[2] & 0b00111111);

    for(int i = 0; i<4; i++){
        destination[i] = '=';
    }
    
    destination[0] = convertir(D);
    destination[1] = convertir(E);
    if(taille_source>1){
        destination[2] = convertir(F);
        if(taille_source>2){
            destination[3] = convertir(G);
        }
    }
    printf("%s\n",destination);
}*/


int encoder_fichier(int source, int destination,int nbBlocsDeTrois){

	int tailleBuffer=nbBlocsDeTrois*3;

	if (source==-1) {
		error("source incorrecte");
		return -1;
	}
	if (destination==-1){
		error("destination incorrecte");
		return -1;
	}

	char bufferSource[tailleBuffer];
	int no=read(source,bufferSource,tailleBuffer);

	if (no==-1){
		error("Le fichier source est un dossier !");
		return -1;
	}

	char bufferDestination[nbBlocsDeTrois*4];
	//printf("Taille bufferDestination : %d\n",nbBlocsDeTrois*4);

	while (no!=0){


		int nbPaquets=encoder_packets(bufferSource,no,bufferDestination);
		/*for (int i=0;i<4*nbBlocsDeTrois;i++){
			printf("%c,",bufferDestination[i]);
		}
		printf("\nno : %d / bufferDestination : %d\n",nbPaquets ,nbBlocsDeTrois);
		printf("==========================\n");*/

		
		no=write(destination,bufferDestination,nbPaquets*4);
		if (no==-1){
			error("Le fichier destination est un dossier !");
			return -1;
		}

		int ya_un_backslash_n=0;
		if (source==0){
			if (no<tailleBuffer){
				ya_un_backslash_n=1;
			}
		}

		if (ya_un_backslash_n==1){
			no=0;
		}else{
			no=read(source,bufferSource,tailleBuffer);
		}
	}

	return 0;
}

int error(char* message){
    fprintf(stderr, message);
    return 0;
}