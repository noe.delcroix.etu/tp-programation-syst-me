#ifndef FONCTIONS_H
#define FONCTIONS_H

#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include "ligne_commande.h"

void execute_ligne_commande();
void affiche_prompt();

#endif