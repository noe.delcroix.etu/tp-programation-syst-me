#include "rat.h"

struct rat rat_produit(struct rat n1, struct rat n2){
	struct rat res;
	res.num=n1.num*n2.num;
	res.den=n1.den*n2.den;
	return res;
}

struct rat rat_somme(struct rat n1, struct rat n2){
	struct rat res;

	res.num=n1.num*n2.den + n2.num*n1.den;
	res.den=n1.den*n2.den;

	return res;
}

struct rat rat_plus_petit(struct rat list[]){
	int i=0;int minidx=0;
	while (list[i].den!=0){
		i++;
		if ((list[minidx].num*list[i].den > list[i].num*list[minidx].den)){
			minidx=i;
		}
	}
	if (list[minidx].den==0){
		list[minidx].num=1;
	}
	return list[minidx];
}