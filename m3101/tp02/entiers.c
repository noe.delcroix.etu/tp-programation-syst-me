#include <stdio.h>
#include "entiers.h"

void afficher(int liste[], int taille){
	printf("[");
	for (int i=0;i<taille;i++){
		printf("%d",liste[i]);
		if (i!=taille-1){
			printf(", ");
		}
	}
	printf("]\n");
}

int somme (int liste[], int taille){
	int somme=0;
	for (int i=0;i<taille;i++){
		somme+=liste[i];
	}
	return somme;
}

void copie_dans(int dest[], int src[], int taille){
	for (int i=0;i<taille;i++){
		dest[i]=src[i];
	}
}

void ajoute_apres(int dest[], int taille_dest, int src[], int taille_src){
	for (int i=0;i<taille_src;i++){
		dest[taille_dest+i]=src[i];
	}
}