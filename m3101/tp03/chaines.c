#include <stddef.h>
#include "chaines.h"

int mon_strlen_tab(char s[]){
	int i=0;
	while (s[i]!='\0')i++;
	return i;
}

int mon_strlen(const char *s){
	int i=0;
	while (*(s+i)!='\0')i++;
	return i; 
}

int mon_strcmp(const char * s1, const char * s2){
	int i=0;
	int l_s1=mon_strlen(s1);
	int l_s2=mon_strlen(s2);
	while (i<=l_s1 && i<=l_s2){	
		if (*(s1+i)<*(s2+i)){
			return -1;
		}else if (*(s1+i)>*(s2+i)){
			return 1;
		}
		i++;
	}
	return 0;
}

int mon_strncmp(const char * s1, const char * s2, int n){
	int i=0;
	int l_s1=mon_strlen(s1);
	int l_s2=mon_strlen(s2);
	while (i<n && i<=l_s1 && i<=l_s2){	
		if (*(s1+i)<*(s2+i)){
			return -1;
		}else if (*(s1+i)>*(s2+i)){
			return 1;
		}
		i++;
	}
	return 0;
}

char *mon_strcat(char *s1, const char *s2){
	int l_s=mon_strlen(s1);
	int i;
	for (i=0;i<mon_strlen(s2)+1;i++){
		*(s1+l_s+i)=*(s2+i);
	}

	return s1;
}

char *mon_strchr(const char *s, int c){
	char* res=NULL;
	int l_s=mon_strlen(s);
	int i;
	for (i=0;i<l_s;i++){
		if (*(s+i)==c){
			return (char*)(s+i);
		}
	}
	return res;
}

/*char *mon_strstr(const char *haystack, const char *needle){
    char* res = NULL;
    int l_needle = mon_strlen(needle);
    int l_haystack = mon_strlen(haystack);
	int i;
    if(l_needle==0){
        return (char*)(haystack);
    }
    for(i=0;i<l_haystack;i++){
        if(*(haystack+i)==*(needle)){
            int k = 0; 
            while(k<l_needle && *(haystack+k+i) == *(needle+k)){
                if(k==l_needle-1){
                    return (char*)(haystack+i);
                }
                k++;
            }
        }
    }         
    return res;
}*/
//Q8
char *mon_strstr(const char *haystack, const char *needle){
    char* res = NULL;
    int l_needle = mon_strlen(needle);
    int l_haystack = mon_strlen(haystack);
	int i;
    if(l_needle==0){
        return (char*)(haystack);
    }
    for(i=0;i<l_haystack;i++){
        if(mon_strncmp(haystack+i, needle,l_needle)==0){
            return (char*)(haystack+i);
        }
    }         
    return res;
}