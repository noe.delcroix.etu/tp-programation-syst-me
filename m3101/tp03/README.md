# TP03 - Chaînes de caractères

# Progression et remarque
Tout a été fait et fonctionne correctement.

# Utilisation
Vous disposez d'un Makefile avec plusieurs règles :

Taper la commande suivante permet de compiler le programme répondant à l'exercice 1 :

    make ex1

Lors de l'execution du programme, plusieurs options peuvent être passé au programme :

* `clean` : permet de supprimer les fichiers .o crées lors de la compilation
* `realclean` : permet de supprimer les fichiers .o ainsi que l'executable crées lors de la compilation


