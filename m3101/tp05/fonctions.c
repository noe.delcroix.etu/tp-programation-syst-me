#include "fonctions.h"
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "../tp04/fonctions.c"

int error(char* message){
    fprintf(stderr, message);
    return 0;
}

int traiter (int f, int *car, int *mot, int *lig){
	const int bufferSize=80;
	char buffer[bufferSize];
	int no=read(f,buffer,bufferSize);
    
    if (no==-1) return -1;

	int estDansMot=0;

	while (no!=0){
		for (int i=0;i<no;i++){
			car[0]++;
			if ( isspace(buffer[i])==0 && estDansMot==0){
				estDansMot=1;
			}
			if (isspace(buffer[i])!=0 && estDansMot==1){
				estDansMot=0;
				mot[0]++;
			}
			if (buffer[i]=='\n'){
				lig[0]++;
			}
		}

        if (f==0 && no<bufferSize){
            no=0;
        }else{
            no=read(f,buffer,bufferSize);
        }
		
	}

    if (estDansMot==1){
        estDansMot=0;
		mot[0]++;
    }
	return 0;
}