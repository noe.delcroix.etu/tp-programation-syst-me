#include "fonctions.h"
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main(int argc, char **argv)
{

    char path[500][500];

    int carOK = 0;
    int motOK = 0;
    int ligneOK = 0;
    int nbPath = 0;
    for(int i = 1; i<argc; i++){
        
        if(argv[i][0]!='-'){
            memcpy(path[nbPath], argv[i], 50);
            nbPath++;
        }
		
        if((strlen(argv[i])==2 && strstr(argv[i], "-c")!=NULL) || (argv[i][0]=='-' && strstr(argv[1], "c"))) carOK = 1;
        if((strlen(argv[i])==2 && strstr(argv[i], "-m")!=NULL) || (argv[i][0]=='-' && strstr(argv[1], "m"))) motOK = 1;
        if((strlen(argv[i])==2 && strstr(argv[i], "-l")!=NULL) || (argv[i][0]=='-' && strstr(argv[1], "l"))) ligneOK = 1;
    }

    

    int totalCar=0,totalMot=0,totalLigne=0;
    int noParam=carOK+motOK+ligneOK;
    int car=0;
    int mot=0;
    int ligne=0;
    
    if (nbPath==0){
        printf("Entrez du texte : \n");
        traiter(0,&car,&mot,&ligne);
        if(ligneOK==1 || noParam==0) printf("%d / ", ligne); 
        if(motOK==1 || noParam==0) printf("%d / ", mot);
        if(carOK==1 || noParam==0) printf("%d /", car);
        printf("\n");
        
    }else{
        for(int i = 0; i<nbPath; i++){
            car=0;mot=0;ligne=0;
            int fd=open(path[i], O_RDONLY);

            if (fd==-1) error("Ce fichier n'existe pas\n");

            if (traiter(fd, &car, &mot, &ligne)==-1) error("Ce fichier est un répertoire\n");
            else{
                printf("%s :     ", path[i]);
                if(ligneOK==1 || noParam==0) printf("%d / ", ligne); 
                if(motOK==1 || noParam==0) printf("%d / ", mot);
                if(carOK==1 || noParam==0) printf("%d / ", car);
                printf("\n");
                   
                totalCar+=car;
                totalMot+=mot;
                totalLigne+=ligne;
            }
        }
            
        if (argc>2){
            printf("total :     ");
            if(ligneOK==1 || noParam==0) printf("%d / ", totalLigne);  
            if(motOK==1 || noParam==0) printf("%d / ", totalMot);
            if(carOK==1 || noParam==0) printf("%d / ", totalCar);
            printf("\n");
            
        }
    }

    
}