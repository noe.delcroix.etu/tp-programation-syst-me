#include "main.h"
#include <stdio.h>
#include "fonctions.h"
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
    if(argc<2){
        printf("Pas assez d'argument\n");
        return 0;
    }

    int miroirOK = 0;
    int saisieOK = 0;
    int nbArgumentSansTiret = 0;
    for(int i = 1; i<argc; i++){
        if(strstr(argv[i], "-")==NULL) nbArgumentSansTiret++;
        else if(strlen(argv[i])==2 && strstr(argv[i], "-m")!=NULL) miroirOK = 1;
        else if(strlen(argv[i])==2 && strstr(argv[i], "-s")!=NULL) saisieOK = 1;
        else if(argv[i][0]=='-' && strstr(argv[1], "m") && strstr(argv[1], "s")){
            miroirOK = 1;
            saisieOK = 1;
        }
        if(nbArgumentSansTiret>1){
            printf("Trop de saisie qui ne sont pas des arguments\n");
            return 0;
        } 
    }

    if(argc==2 && miroirOK==0 && saisieOK==1){
        char buffer[ 255 ];
        printf("Veuillez saisir un mot = ");
        scanf(buffer);
        char* saisiePointeur=saisie(buffer);
        printf("Vous avez écris = %s\n", saisiePointeur);
        free(saisiePointeur);
    }else if(argc==3 && miroirOK==1 && saisieOK==0){
        char * miroirPointeur=miroir(argv[2]);
        printf("Le miroire de votre saisie = %s\n", miroirPointeur);
        free(miroirPointeur);
    }else if(nbArgumentSansTiret==0 && miroirOK==1 && saisieOK==1){
        char buffer[ 255 ];
        printf("Veuillez saisir le mot que vous souhaitez inverser = ");
        scanf(buffer);
        char* saisiePointeur=saisie(buffer);
        char* miroirPointeur=miroir(saisiePointeur);
        printf("Le miroir de votre saisie précédente est = %s\n", miroirPointeur);
        free(saisiePointeur);
        free(miroirPointeur);
    }else{
        printf("Mauvaise Utilisation\n");
    }
    return 0;
}

int contientNimp(char* chaine)
{
    int res = 0;

    int idx = 1;
    while(chaine[idx] != '\0'){
        for(int i = 0 ; i<26 ; i++){
            if(chaine[idx]!='m' && chaine[idx]!='s'){
                res = 1;
                return res;
            }
        }
        idx ++;
    }

    return res;
}