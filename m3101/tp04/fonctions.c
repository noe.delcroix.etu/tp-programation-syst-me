#include "fonctions.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "tests.h"

char * miroir(const char *s){
	int len=strlen(s);
	char* res=malloc( (len+1) *sizeof(char));
	int i;

	for (i=0;i<len;i++){
		*(res+i)=*(s+(len-i-1));
	}

	res[len]='\0';
	return res;
}

/*char * saisie(){

	char* res,* temp;
	char c;
	int cpt=0,i;

	while (c!='\n'){
		c=getchar();

		temp=malloc(cpt*sizeof(char));
		for (i=0;i<cpt;i++){
			*(temp+i)=*(res+i);
		}

		if (!isspace(c)){
			cpt++;
			res=malloc(cpt*sizeof(char));
			for (i=0;i<cpt-1;i++){
				*(res+i)=*(temp+i);
			}
			*(res+i)=c;
		}

	}
	res[i]='\0';
	free(temp);
   	return res;
}*/

/*char * saisie(){
	
	char* res=malloc(sizeof(char));
	char c = 1;
	int i=0;

	while (1) {
		c=getchar();
		if (isspace(c) || c == EOF) break;
		res[i]=c;
		i++;
		res=realloc(res, i+1);
	}
	res[i]='\0';
   	return res;
}*/

char * saisie(){
    int nb = 0;
    int max = 10000;
    char * chaine = malloc(max * sizeof(char));
    char c = getchar();

    while(c != EOF && c!= '\n' && isspace(c) == 0){
        nb++;
        if (nb % max == 0)
            chaine = realloc(chaine, max + nb);
        chaine[nb - 1] = c;
        c = getchar();
    }
    
    if (chaine == NULL)
    {
        free(chaine);
        return NULL;
    }
    *(chaine + nb) = '\0';
    chaine = realloc(chaine, nb+1);
    return chaine;
}

